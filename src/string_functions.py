def print_welcome_in_our_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście - {city}!")


def generate_email_address_for_a_new_joiner(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"


if __name__ == '__main__':
    # Zadanie 1
    print_welcome_in_our_city("Anna", "Nowa Sól")

    # Zadanie2
    print(generate_email_address_for_a_new_joiner("Janusz", "Nowak"))
    print(generate_email_address_for_a_new_joiner("Barbara", "Kowalska"))
