# funkcja która nic nie zwraca ale drukuje markę samochodu
def print_a_car_brand_name():
    print("Audi")


print_a_car_brand_name()

result_of_function_which_does_not_return_anything = print_a_car_brand_name()  # przypisuję zmienną do wyniku tej funkcji
print(result_of_function_which_does_not_return_anything)  # funkcja nic nie zwraca ale chcemy się upewnić


# funkcja nic nie zwraca ale drukuje wybraną liczbę pomnożoną przez 3


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


print_given_number_multiplied_by_3(10)


# funkcja nic nie drukuje ale zwraca pole koła. zeby wydrukowac wynik trzeba go przypisać do zmiennej a potem zrobić print
def calculate_area_of_a_circle(radius):
    return 3.1415 * radius ** 2  # nie musi byc nawiasów


area_of_a_circle = calculate_area_of_a_circle(10)
print(area_of_a_circle)


#
def calculate_area_of_a_triangle(bottom, height):
    return 0.5 * bottom * height


area_of_triangle = calculate_area_of_a_triangle(3, 4)
print(area_of_triangle)
