first_name = "Anna"
last_name = "Mudlaff"
email = "anna.mudlaff19@gmail.com"

print("Mam na imię", first_name)  # wydrukuje powitanie

print("Mam na imię", first_name, "Nazywam się", last_name)  # zdania nie są oddzielone kropką

print("Mam na imię", first_name, ".", "Nazywam się", last_name, ".")  # przed kropkami nie powinno być spacji

print("Mam na imię", first_name, ".", "Nazywam się", last_name, ".",
      sep="")  # usuwamy spacje przed kropkami ale też wszystkie inne

print("Mam na imię ", first_name, ". ", "Nazywam się ", last_name, ". ",
      sep="")  # dodane spacje tam gdzie ich potrzebujemy i w końcu to wygląda

# zeby w tym przypadku wydrukowac cos co ma sens potrzeba duzo kodu i zabawy. Zamiast tego można zrobic concatenate:

my_bio = "Mam na imię " + first_name + ". " + "Nazywam się " + last_name + ". "  # już to jakoś wygląda ale można jeszcze prościej
print(my_bio
      )
my_bio = "Mam na imię " + first_name + ". Nazywam się " + last_name + ". "  # kropkę przerzucoono do tzeciej pozycji
print(my_bio)

# concatenate nie daje spacji pomiędzy pozycjami
# w dalszym ciągu wymaga to od nas duzo pracy i dużo kodu
# lepszym rozwiązaniem są F-string

my_bio_using_f_string = "Mam na imię {first_name}. Nazywam się {last_name}. Mój email to {email}."
print(my_bio_using_f_string)

my_bio_using_f_string = f"Mam na imię {first_name}. Nazywam się {last_name}. Mój email to {email}."
print(my_bio_using_f_string)

# dzięki f przed znakiem " możemy wprowadzać zmienne do środka stringa

my_bio_using_f_string = f"Mam na imię {first_name}.\nNazywam się {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

# wprowadzając \n dzielimy stringa na osobne linie
# UWAGA: musi być to backslash

# f-string działa nie tylko na zmiennych:
print(f"Wynik operacji mnożenia 4 przez 5 wynosi {4 * 5}")

# ALGEBRA ########

area_of_a_circle_with_radius_5_version1 = 3.14 * 5 ** 2  # ** podnosi liczbę do potęgi a * mnoży
area_of_a_circle_with_radius_5_version2 = 3.14 * 5 * 5  # wychodzi na to samo

print(f"Pole koła z potęgą wynosi {area_of_a_circle_with_radius_5_version1}")
print(f"Pole koła z mnożeniem wynosi {area_of_a_circle_with_radius_5_version2}")

# można użyć zmiennych zamiast na sztywno ustalać dane:

circle_radius = 5
area_of_a_circle_with_radius_5_version = 3.14 * circle_radius ** 2
print(f"Pole koła z promieniem {circle_radius} wynosi {area_of_a_circle_with_radius_5_version}")

# uwaga bo Python zachowuje poprawną kolejność działań

good_practice1 = 3 * 3 * 3
good_practice2 = 3 * 3 ** 3
good_practice3 = (3 * 3) ** 3

print(good_practice1)
print(good_practice2)
print(good_practice3)
