animals = [
    {"name": "Pusia", "kind": "dog", "age": 15},
    {"name": "Zgładziło", "kind": "cat", "age": 9},
    {"name": "Bunia", "kind": "guinea pig", "age": 8},
    {"name": "Franka", "kind": "squirrel", "age": 3},
]

address_from_excersise_2 = [
    {"city": "Krakow", "street": "Śliczna", "house_number": 1, "post_code": "31-123"},
    {"city": "Wieliczka", "street": "Elegancka", "house_number": 2, "post_code": "30-001"},
    {"city": "Dobczyce", "street": "Fajna", "house_number": 3, "post_code": "30-321"},
]

if __name__ == '__main__':
    print("name of last animal:")
    print(animals[-1]["name"])
    animals[1]["age"] = 2
    print(animals[1]["age"])
    print(len(animals))

    print(address_from_excersise_2[-1]["post_code"])
    print(address_from_excersise_2[1]["city"])
    address_from_excersise_2[0]["street"] = "Ładna"
    print(address_from_excersise_2)
