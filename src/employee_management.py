import random
# import time  # importuje mozliwość korzystania z unix time
import uuid


def generate_random_email():
    domain = "example.com"
    names_list = ["kate", "john", "veronica", "emma", "kate"]
    random_name = random.choice(names_list)
    # suffix = time.time()  # generuje unix time
    suffix = str(uuid.uuid4())
    return f'{random_name}.{suffix}"@"{domain}'


def generate_random_number():
    return random.randint(1, 50)


def generate_random_boolean():
    return random.choice([True, False])
    # return bool(random.getrandbits(0, 1))


def return_dictionary_with_employee_email_seniority_in_years_and_sex():
    return {
        "email": generate_random_email(),
        "seniority_years": generate_random_number(),
        "female": generate_random_boolean(),
    }


def generate_list_of_dictionaries_with_random_personal_data(number_of_dictionaries):
    list_of_dictionaries_with_random_personal_data = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries_with_random_personal_data.append(
            return_dictionary_with_employee_email_seniority_in_years_and_sex())
    return list_of_dictionaries_with_random_personal_data


def generate_list_of_employees_with_seniority_longer_than_10_years(list_of_employees):
    senior_employees = []
    for employee in list_of_employees:
        if employee["seniority_years"] > 10:
            senior_employees.append(employee["email"])
    return senior_employees


def generate_list_of_employees_with_seniority_longer_than_n_years(list_of_employees, seniority_years_margin):
    senior_employees = []
    for employee in list_of_employees:
        if employee["seniority_years"] > seniority_years_margin:
            senior_employees.append(employee["email"])
    return senior_employees


def get_list_of_female_employees(list_of_employees):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] is True:
            filtered_employees.append(employee)
    return filtered_employees


def get_list_of_employees_filtered_by_gender(list_of_employees, is_female):
    filtered_employees = []
    for employee in list_of_employees:
        if employee["female"] == is_female:
            filtered_employees.append(employee)
    return filtered_employees


if __name__ == '__main__':
    print(return_dictionary_with_employee_email_seniority_in_years_and_sex())
    # print(generate_list_of_dictionaries_with_random_personal_data(10))
    test_employees = generate_list_of_dictionaries_with_random_personal_data(20)
    print(test_employees)
    test_senior_employee_emails = generate_list_of_employees_with_seniority_longer_than_n_years(test_employees, 50)
    print(test_senior_employee_emails)
    test_female_employees = get_list_of_female_employees(test_employees)
    print(test_female_employees)
    test_male_employees = get_list_of_employees_filtered_by_gender(test_employees, False)
    print(test_male_employees)
    print(len(test_employees))
    print(len(test_male_employees))
    print(len(test_female_employees))
