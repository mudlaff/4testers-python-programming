first_name = "Anna"
last_name = "Mudlaff"
my_age = 32

# print(first_name)
# print(last_name)
# print(age)

# I am defining variables describing my best friend
friends_name = "Olaff"
friends_age = 33
friends_animals_owned = 0
friends_driving_license = True
friendship_duration = 31

my_bank_account_total = 6_000_000
my_friends_account_total = 5e12

print(my_bank_account_total)
print(my_friends_account_total)

# Below I describe my friendship details as a set of variables
friend_name = 'Tomek'
friend_age = 33
friend_number_of_animals = 2
friend_has_driving_license = False
friendship_duration_in_years = 3.5

print(friends_name, friends_age, friends_animals_owned, friends_driving_license, friendship_duration)

print("Friend's name:", friends_name)  # oddzielone spacją
print("Friend's name:", friends_name, sep='\t')  # oddzielone tabulacją
print(friends_name, friends_age, friends_animals_owned, friends_driving_license, friendship_duration,
      sep=', ')  # oddzielone przecinkami i spacjami
