my_favourite_movies = ["Titanic", "Primal Fear", "Dirty Dancing", "Harry Potter", "Kogel Mogel"]

last_movie = my_favourite_movies[-1]
my_favourite_movies.append("LOTR")
my_favourite_movies.append("Pearl Harbour")
print(len(my_favourite_movies))

emails = ["a@example.pl", "b@example.pl"]
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append("cde@example.pl")

my_friend = {
    "name": "Olaff",
    "age": 33,
    "hobby": ["Taniec", "literatura"],
}

friend_hobbies = my_friend["hobby"]
print("Hobbies of my friend: ", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")

my_friend["hobby"].append("Football")
print(my_friend)

my_friend["married"] = True
my_friend["age"] = 18
print(my_friend)
