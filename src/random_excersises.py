import string
import random


# Get random password of length 8 with letters, digits, and symbols


def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


if __name__ == '__main__':
    print(generate_login_data("andor@starwars.com"))
    print(generate_login_data("mando@starwars.com"))
    print(generate_login_data("kenobi@starwars.com"))

    user = {'email': 'user@example.com', 'age': 22, 'weight': 67}
    for v in user.values():
        print('Value:', v)
