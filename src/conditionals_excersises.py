# Zadanie 1
def display_speed_information(speed):
    # if speed <= 50:
    #     print("your speed is below limit")
    # else:
    #     print("slow down")
    # lepiej tak:
    if speed > 50:
        print("slow down")
    else:
        print("your speed is below limit")


# Zadanie 2


def check_if_the_weather_is_normal(celsius_degree, pressure_in_hectopascals):
    if celsius_degree == 0 and pressure_in_hectopascals == 1013:
        print(True)
    else:
        print(False)


# Zadanie 3
# najpierw definiuję funckję, która policzy mi wartośc mandatu. Chodzi o to żeby funkcje były jak najprostsze.
# Lepiej mie c kilka funkcji niż jedną rozbudowaną


def calculate_speeding_fine(speed):
    return 500 + (speed - 50) * 10


# druga funkcja:


def print_value_of_speeding_fine_in_build_up_area(speed):
    print(f"Your speed was: {speed}")
    if speed > 100:
        print("You just lost your driving license!")
    elif speed > 50:
        print(f"You just got a speeding ticket. Fine amount: {calculate_speeding_fine(speed)}")
    else:
        print("Your speed is fine")


# Zadanie 4


def return_grade_description(grade):
    if not (isinstance(grade, float) or isinstance(grade, int)):
        return "Serio? Miały być liczby"
    elif grade < 2 or grade > 5:
        return "N/A"
    elif grade >= 4.5:
        return "bardzo dobry"
    elif grade >= 4:
        return "dobry"
    elif grade >= 3:
        return "dostateczny"
    else:
        return "niedostateczny"


if __name__ == '__main__':
    # rozwiązanie 1
    print("Rozwiązanie 1")
    display_speed_information(49)
    display_speed_information(50)
    display_speed_information(51)

    # rozwiązanie 2
    print("Rozwiązanie 2")
    check_if_the_weather_is_normal(0, 1013)
    check_if_the_weather_is_normal(1, 1013)
    check_if_the_weather_is_normal(1, 1014)
    check_if_the_weather_is_normal(0, 1014)

    # rozwiązanie 3
    print("Rozwiązanie 3")
    print_value_of_speeding_fine_in_build_up_area(49)
    print_value_of_speeding_fine_in_build_up_area(50)
    print_value_of_speeding_fine_in_build_up_area(51)
    print_value_of_speeding_fine_in_build_up_area(100)

    # rozwiązanie 4
    print("Rozwiązanie 4")
    print(return_grade_description(2))
    print(return_grade_description(3))
    print(return_grade_description(4))
    print(return_grade_description(4.5))
    print(return_grade_description(5))
    print(return_grade_description(5.8))
    print(return_grade_description(1))
    print(return_grade_description("a"))
