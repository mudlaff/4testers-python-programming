# listy są złożonymi typami zmiennych
# listy przechowują dane

shopping_list = ["oranges", "water", "chicken", "potatoes",
                 "washing liquid"]  # listę definiujemy z kwadratowymi nawiasami

# numerację elementów listy zaczynami nie od 1 ale od 0
# Z tego względu ilość elementów listy to długość listy - 1

print(shopping_list[0])  # drukujemy pierwszy element z listy
print(shopping_list[2])  # drukujemy trzeci element z listy
print(shopping_list[-1])  # drukujemy ostatni element z listy
print(shopping_list[-2])  # drukujemy przed ostatni element z listy

# UWAGA: od lewej zaczynamy numeracje od 0 a od prawej od -1

shopping_list.append("lemons")  # dodajemy nową pozycję do listy
print(shopping_list)  # teraz juz mamy dodane cytryny
print(shopping_list[-1])  # zmienił się ostatni element

number_of_items_to_buy = len(
    shopping_list)  # funkcja len liczy ile mamy elementów na liście. Przypisaliśmy tę wartość do zmiennej
print(number_of_items_to_buy)

first_three_shopping_items = shopping_list[
                             0:3]  # chcemy wyświetlić tylko 3 pierwsze elementy. Do zmiennej przypisujemy nową listę
# UWAGA: pamietamy, że numerację pozycji od lewej strony zaczynamy od 0. Indeksy oddzelamy dwukropkiem.
# UWAGA: zakres jest prawostronnie otwarty. To znaczy, że drugi indeks nie zostanie uwzględniony w nowej liście.
# Dlatego żeby wyświetlić 3 pierwsze elementy musimy podać zakres czterech
# ostatni wyświetlony element to będzie element poprzedzajacy element z drugiego indeksu
print(first_three_shopping_items)

# SŁOWNIKI ###########
# słowniki (#-mapy albo mapy ) są drugim typem złożonych strultur danych
# słowniki podobnie jak listy są wieloelementowym typem zmiennych
# ale w przeciwieństwie do list nie mają indeksów i elementów tlyko klucze i wartości

animal = {
    "name": "Pusia",
    "kind": "dog",
    "age": 14,
    "male": False

}
# słownik definiujemy za pomocą klamr: {}
# dobrze zrobić enter po pierwszej kalmrze żeby lepiej widzieć
# pierwsza pozycja to klucz a druga to wartość. Odzielamy je dwukropkiem
# aby wprowadzić nową parę musimy je oddzielić przecinkiem od poprzednich

# aby wyprintować jakąś wartośc (np wiek psa) musimy ją wyciągnąć po przypisanym do niej kluczu:

dog_age = animal["age"]
print(f"Dog age: {dog_age}")
dog_name = animal["name"]
print(f"Dog name: {dog_name}")

# może zmienić jakąś wartość:
animal["age"] = 10
print(animal)

print(f"Dog age: {dog_age}")  # tutaj się drukuje stary wiek nie wiem czemu

# można dodawać nowe pary kluczy i wartości
animal["best_friend"] = "Anna"
print(animal)
