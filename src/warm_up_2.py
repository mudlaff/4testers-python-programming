def square_of_a_random_number(input_number):
    return input_number ** 2


def celcius_to_fahrenheit(input_number):
    print(input_number * 1.8 + 32)


def calculate_volume_of_cuboid(side1, side2, side3):
    return side1 * side2 * side3


if __name__ == '__main__':  # to co mamy w bloku musi być wcięte, trzeba zaznaczyć wszystko co chcemy mieć w bloku i klikamy tab
    # Zadanie 1
    print(square_of_a_random_number(0))
    print(square_of_a_random_number(16))
    print(square_of_a_random_number(2.55))

    # Zadanie 2
    celcius_to_fahrenheit(2)

    # Zadanie 3
    print(calculate_volume_of_cuboid(3, 5, 7))
