def count_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)


def count_average_of_number(a, b):
    return (a + b) / 2


def print_player_features(p_nick, p_type, p_exp):
    return f"The_player", p_nick, "is_type_of_a", p_type, "and_has", p_exp, "EXP"


if __name__ == '__main__':
    # Zadanie 1
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]

    january_average = count_average_of_list_numbers(january)
    february_average = count_average_of_list_numbers(february)
    bimonthly_average_from_january_to_february = (january_average + february_average) / 2
    print(bimonthly_average_from_january_to_february)

    player_account = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000

    }

    nick_name = player_account["nick"]
    account_typ = player_account["type"]
    points = player_account["exp_points"]

    print(print_player_features(nick_name, account_typ, points))
