from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(0, 40):
        print("Hello", i + 1)


def print_positive_numbers(start, stop):
    for i in range(start, stop + 1):
        print(i)


def print_positive_numbers_divisionable_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:  # czy jest podzielna przez 0
            print(i)


def print_n_random_numbers_from_range_from_1_to_1000(n):
    for i in range(n):
        print(f'loop {i}:', randint(1, 1000))  # losowa liczba od 1 do 1000


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def print_square_roots_of_numbers_roots(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        sqrt_of_numbers = round(sqrt(number), 2)
        square_roots.append(sqrt_of_numbers)
    return square_roots


if __name__ == '__main__':
    print("Rozwiązanie1")
    print_hello_40_times()
    print("Rozwiązanie2")
    print_positive_numbers(8, 45)
    print("Rozwiązanie3")
    print_positive_numbers_divisionable_by_7(0, 100)
    print("Rozwiązanie4")
    print_n_random_numbers_from_range_from_1_to_1000(8)
    print("Rozwiązanie5")
    list_of_measurment_numbers = [1, 9, 34, 65, 26, 98]
    print_square_roots_of_numbers(list_of_measurment_numbers)
    print("Rozwiązanie6")
    list_of_numbers_to_be_square_rooted = [1, 9, 16, 25, 36, 49, 64]
    print(print_square_roots_of_numbers_roots(list_of_numbers_to_be_square_rooted))
