import random  # required to use random.choice and random.randint
from datetime import date  # required to get current year


def get_random_female_name_from_a_list(list_of_female_names):
    random_female_firstname = random.choice(list_of_female_names)
    return random_female_firstname


def get_random_male_name_from_a_list(list_of_male_names):
    random_male_firstname = random.choice(list_of_male_names)
    return random_male_firstname


def get_random_last_name_from_a_list(list_of_last_names):
    random_last_name = random.choice(list_of_last_names)
    return random_last_name


def get_random_country_from_a_list(existing_list_of_countries):
    random_country = random.choice(existing_list_of_countries)
    return random_country


def create_an_email(name, last_name):
    domain = "@example.com"
    return f'{name.lower()}.{last_name.lower()}{domain}'


def generate_random_age():
    random_age = random.randint(5, 45)
    return random_age


def check_if_adult(generated_random_age):
    if generated_random_age >= 18:
        return True
    else:
        return False


def calculate_birth_year(generated_random_age):
    year = date.today().year
    return year - generated_random_age


def create_dictionary_with_female_personal_data():
    name = get_random_female_name_from_a_list(list_of_female_fnames)
    last_name = get_random_last_name_from_a_list(list_of_surnames)
    country = get_random_country_from_a_list(list_of_countries)
    email = create_an_email(name, last_name)
    age = generate_random_age()
    if_adult = check_if_adult(age)
    birth_year = calculate_birth_year(age)
    return {
        "firstname": name,
        "lastname": last_name,
        "country": country,
        "email": email,
        "age": age,
        "adult": if_adult,
        "birth_year": birth_year,
    }


def create_dictionary_with_male_personal_data():
    name = get_random_male_name_from_a_list(list_of_male_fnames)
    last_name = get_random_last_name_from_a_list(list_of_surnames)
    country = get_random_country_from_a_list(list_of_countries)
    email = create_an_email(name, last_name)
    age = generate_random_age()
    if_adult = check_if_adult(age)
    birth_year = calculate_birth_year(age)
    return {
        "firstname": name,
        "lastname": last_name,
        "country": country,
        "email": email,
        "age": age,
        "adult": if_adult,
        "birth_year": birth_year,
    }


def create_list_of_dictionaries_with_personal_data():
    list_of_dictionaries_with_personal_data = []
    for i in range(1, 6):
        list_of_dictionaries_with_personal_data.append(create_dictionary_with_female_personal_data())
        list_of_dictionaries_with_personal_data.append(create_dictionary_with_male_personal_data())
    return list_of_dictionaries_with_personal_data


def print_greetings_note_from_everybody_from_the_list(list_of_everybody):
    for person in range(len(list_of_everybody)):
        print(
            f"Hi! I'm {list_of_everybody[person]['firstname']} {list_of_everybody[person]['lastname']}. I come from {list_of_everybody[person]['country']} and I was born in {list_of_everybody[person]['birth_year']}")


if __name__ == '__main__':
    list_of_female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    list_of_male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    list_of_surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    list_of_countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

    # age_of_a_person = generate_random_age()
    # print(age_of_a_person)
    # print(check_if_adult(age_of_a_person))
    # print(calculate_birth_year(age_of_a_person))

    # print(create_dictionary_with_female_personal_data())
    # print(create_dictionary_with_male_personal_data())
    list_of_people = create_list_of_dictionaries_with_personal_data()
    print(list_of_people)
    print_greetings_note_from_everybody_from_the_list(list_of_people)
